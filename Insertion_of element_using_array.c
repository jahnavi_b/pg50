#include <stdio.h>
int main()
{
	int a[20],n,i,pos,value;
	printf("Enter number of elements : \n");
	scanf("%d",&n);
	printf("Enter the %d elements : ",n);
	for(i=0;i<n;i++)
	{
		printf("\n a[%d] = ",i);
		scanf("%d",&a[i]);
	}
	printf("\nEnter the element to be inserted : ");
	scanf("%d",&value);
	printf("\nEnter the position in which the element is to be inserted : ");
	scanf("%d",&pos);
	for(i=n-1;i>=pos-1;i--)
	{
		a[i+1]=a[i];
	}
	a[pos-1]=value;
	n++;
	printf("\nThe new array after insertion of the element is :");
	for(i=0;i<n;i++)
	{
		printf("\n a[%d] = %d",i,a[i]);
	}
	return 0;	
}